import Util from "./utils.js"

const Modals = (() => {

  var Default = {
    container: document.getElementById("modal-panel-floating"),
    showOnCreate: true,
    btn: {
      hClose: true,
      close: true,
      ok: true,
    },
    title: "modal",
    saveCallback: null
  }

  var modals = []

  class Modal {
    constructor(content, config) {
      this.config = {
        ...Default,
        ...config
      };
      console.log(this.config)
      this.content = content;
      if (this.config.showOnCreate) {
        this.draw()
      }
    }

    draw() {
      let modal = Util.createElementWithAttributes("div", "ad-modal", null, {"aria-label": "modal element"});
      let header = modal.createChild("div", "ad-modal-header");
      this.elements = {
        modal: modal,
        header: header,
        title: header.createChild("span", "ad-modal-header-title", this.config.title),
        body: modal.createChild("div", "ad-modal-body"),
        footer: modal.createChild("div", "ad-modal-footer")
      }
      if (this.config.btn.hClose) {
        this.elements.header.createChild("button", "ad-modal-header-close ad-icon--close").addEvent("click", () => {this.close()});
      }
      if (this.config.btn.close) {
        this.elements.footer.createChild("button", "ad-modal-footer-close", "close").addEvent("click", () => {this.close()});
      }
      if (this.config.btn.ok) {
        this.elements.footer.createChild("button", "ad-modal-footer-ok", "ok").addEvent("click", () => {this.save()});
      }
      if (this.content) {
        this.elements.body.appendChild(this.content);
      }
      this.config.container.appendChild(this.elements.modal);
    }

    show() {
      this.elements.modal.classList.toggle("ad-modal-visible", true);
    }

    hide() {
      this.elements.modal.classList.toggle("ad-modal-visible", false);
    }

    close() {
      this.elements.modal.remove();
      Modals.remove(this);
    }

    save() {
      if (this.config.saveCallback && typeof this.config.saveCallback == "function") {
        this.config.saveCallback(this);
        this.close();
      }
    }
  }

  const Modals = {
    create(content, config) {
      let modal = new Modal(content, config);
      modals.push(modal);
      return modal
    },

    remove(modal) {
      let index = modals.indexOf(modal);
      if (index > -1) {
        modals.splice(index, 1);
      }
    },

    getAll() {
      return modals
    },

    setContainer(container) {
      Default.container = container;
    }
  }

  return Modals
})();

export default Modals