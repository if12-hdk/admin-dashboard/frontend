import Util from "./utils.js"
import DateTime from "./datetime.js"

const Notification = (() => {

  const Default = {
    places: {
      floating: {
        elem: document.getElementById("notification-panel-floating"),
        autohide: true,
        timeout: 4000,
        elementType: "ad-notification"
      }
    }
  }

  class Notification {

    constructor(config) {
      this.config = {
        ...Default,
        ...config
      };
    }
    
    createNew(type, title, text) {
      for (let place in this.config.places) {
        if (this.config.places.hasOwnProperty(place)) {
          place = this.config.places[place];
          let notification = document.createElement(place.elementType);
          notification.type = type;
          notification.autohide = place.autohide;
          notification.title = title;
          notification.innerHTML = text;
          notification.delay = place.timeout;
          place.elem.appendChild(notification);
        }
      }
    }

    createNewError(error) {
      this.createNew("error", error.Code, error.Message)
    }

    createNewWarning(error) {
      this.createNew("warning", error.Code, error.Message)
    }

    setPlaceContainer(place, container) {
      Default.places[place].elem = container;
    }
  }


  class ADNotification extends HTMLElement {
    constructor() {
      super();
      // initialize events
      this._show = new Event('ad.notification.show');
      this._shown = new Event('ad.notification.shown');
      this._hide = new Event('ad.notification.hide');
      this._hidden = new Event('ad.notification.hidden');
    }

    connectedCallback() {
      // get values from atributes or use defaults
      this._type = ((this.hasAttribute('type')) ? this.getAttribute('type') : 'info');
      // (ensure type is always set)
      this.setAttribute('type', this._type);
      this._autohide = ((this.hasAttribute('autohide')) ? true : false);
      this._delay = ((this.hasAttribute('delay')) ? parseInt(this.getAttribute('delay')) : 4000);
      this._duration = ((this.hasAttribute('duration')) ? parseInt(this.getAttribute('duration')) : 400);
      this._title = ((this.hasAttribute('title')) ? this.getAttribute('title') : this._type);
      this._message = ((this.innerHTML != '') ? this.innerHTML : '');
      // references to child elements (key: value)
      this.elements = null;
      // timeout between states
      this._timeout = null;
      // current status of the notification
      // -1: not rendered
      //  0: hidden
      //  1: showing (show)
      //  2: shown
      //  3: hiding (hide)
      this._status = -1;

      // set the css transition duration
      this.style = "transition-duration: " + (this._duration / 1000) + "s !important;"
      
      // this is not good style and should get tested!!
      this.show();
    }

    // getter and setter for attributes
    get type() {
      return this._type;
    }
    set type(v) {
      this._type = v;
      this.setAttribute('type', v);
    }

    get autohide() {
      return this._autohide;
    }
    set autohide(v) {
      this._autohide = v;
      this.setAttribute('autohide', v);
    }

    get delay() {
      return this._delay;
    }
    set delay(v) {
      this._delay = v;
      this.setAttribute('delay', v);
    }

    get duration() {
      return this._duration;
    }
    set duration(v) {
      this._duration = v;
      this.setAttribute('duration', v);
      this.style = "transition-duration: " + (v / 1000) + "s !important;"
    }

    get title() {
      return this._title;
    }
    set title(v) {
      this._title = v;
      this.setAttribute('title', v);
    }

    get message() {
      return this._message;
    }
    set message(v) {
      this._message = v;
      this.elements.nBody.innerHTML = v;
    }

    // visibility has to be set via hide() and show()
    get hidden() {
      return this._status;
    }

    // calculate the height of the notification
    get height() {
      return this.elements.nHeader.offsetHeight + this.elements.nBody.offsetHeight + 1;
    }

    // generate DOM structure
    render () {
      console.log("render", this._status);
      // set status to hidden
      this._status = 0;
      while (this.lastChild) {
        this.removeChild(this.lastChild);
      }
      let nHeader = this.createChild("div", "ad-n-header").addEvent("click", this.handleTitleClick).referenceTo("notification", this);
      let nBody = this.createChild("div", "ad-n-body", this._message).referenceTo("notification", this);
      this.elements = {
        nHeader: nHeader,
        nIcon: nHeader.createChild("div", "ad-n-icon ad-icon--" + this._type + " ad-n-button-icon"),
        nTitle: nHeader.createChild("strong", "ad-n-title", this._title),
        nTime: nHeader.createChild("small", "ad-n-time").referenceTo("notification", this),
        nButton: nHeader.createChild("button", "ad-n-close ad-icon--close ad-n-button-icon").addEvent("click", this.handleCloseClick).referenceTo("notification", this),
        nBody: nBody
      }
      let datetime = new DateTime(this.elements.nTime, moment().unix(), {interval: 1000});
    }

    // show the notification
    show() {
      // if not initialized do now
      if (this._status == -1) {
        this.render();
      }
      // if not starting from the hidden status: clear timeout and proceed normally
      if (this._status != 0) {
        clearTimeout(this._timeout);
      }
      this._status = 1;
      this.style.height = 0;
      this.classList.remove("hide");
      this.classList.add("fade", "show");
      this.dispatchEvent(this._show);
      requestAnimationFrame(() => this.style.height = this.height + "px");
      this._timeout = setTimeout(() => this.shown(), this._duration);
    }

    shown() {
      // if not starting from the show status: clear timeout and proceed normally
      if (this._status != 0) {
        clearTimeout(this._timeout);
      }
      this._status = 2;
      this.classList.remove("fade", "hide");
      this.classList.add("show");
      this.style.height = "auto";
      this.dispatchEvent(this._shown);
      // check whether notification should be hidden automatically
      if (this._autohide) {
        this._timeout = setTimeout(() => this.hide(), this._delay);
      }
    }

    hide() {
      // if not starting from the shown status: clear timeout and proceed normally
      if (this._status != 1) {
        clearTimeout(this._timeout);
      }
      this._status = 3;
      this.style.height = this.height + "px";
      requestAnimationFrame(() => {
      this.classList.remove("show");
      this.classList.add("fade", "hide");
      this.dispatchEvent(this._hide);
      requestAnimationFrame(() => this.style.height = 0);
      });
      this._timeout = setTimeout(() => this.hidden(), this._duration);
    }

    hidden() {
      // if not starting from the hide status: clear timeout and proceed normally
      if (this._status != 2) {
        clearTimeout(this._timeout);
      }
      this.classList.remove("show", "fade");
      this.classList.add("hide");
      this.style.height = 0;
      this._status = 0;
      this.dispatchEvent(this._hidden);
    }

    // handler for close button
    handleCloseClick(e) {
      // stop the bubbeling of the event
      e.stopPropagation();
      console.log("close", this.notification);
      // hide the notification
      this.notification.hide();
    }

    // handler for title click (stops autohide)
    handleTitleClick(e) {
      console.log("title", this._status);
      // sets autohide to false
      this.notification.autohide = false;
      // make sure the notification is visible
      this.notification.show();
    }
  }

  // register the custom element
  customElements.define("ad-notification", ADNotification);

  return Notification
})();

export default Notification