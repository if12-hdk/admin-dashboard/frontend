import authenticate, {redirect} from "../components/auth.js"
import Util from "../components/utils.js"
import App from "../components/app.js"

authenticate();

let app = new App({draw: {
  removebodyContent: false,
  navTop: false,
  navSide: false,
  contentWrapper: false,
  notificationFloating: true,
  modalFloating: false
}});

window.onload = function() {
  document.getElementById("login-form").customValidate(function(event) {
    event.preventDefault();
    event.stopPropagation();
    var encoded = btoa(document.getElementById("username").value + ":" + document.getElementById("password").value)
    var headers = [{"key":"Authorization", "value":"Basic " + encoded}]
    let r = Util.RequestAsync("POST", app.adapiUrl("/login"), function (res) {
      console.log(res)
      if (res.Errors != undefined) {
        res.Errors.forEach(error => {
          app.notification.createNewError(error)
        });
      } else {
        redirect();
      }
    }, null, headers)
    console.log(r)
  });
}