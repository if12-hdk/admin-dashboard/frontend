import authenticate from "../components/auth.js"
import Util, {Link} from "../components/utils.js"
import List from "../components/list.js"
import Issue, {IssueSkeleton} from "../components/issue.js"
import App from "../components/app.js"

if (authenticate()) {
  let app = new App({}, (app) => {

    let icont = document.getElementById("issues-container");
    console.log(icont)
    let list = new List(icont, {skeletonFunction: IssueSkeleton.drawList, loadOnConstruct: false}, handleListChange);
    app.addUserControl(list);
    Util.RequestAsync("GET", app.adapiUrl("/issues/stats"), (data) => {
      list.entitiesCount = data.count;
      app.loadControlValues("list");
      list.load();
    });
  });

  function handleListChange(vals) {
    let dat = {from: vals.listPageNumber * vals.listPageEntitiesCount, count: vals.listPageEntitiesCount}
    Util.RequestAsync("POST", app.adapiUrl("/api/issues"), (data) => {
      if (data.Errors) {
        data.Errors.forEach(error => {
          app.notification.createNewError(error);
        });
      } else {
        vals.list.removeEntities();
        data.forEach(issue => {
          app.Entities[issue._adid] =  new Issue(issue);
          vals.list.addEntity(app.Entities[issue._adid].drawList());
        });
      }
    }, dat);
  }
}