import authenticate from "../components/auth.js"
import Util, {Link, initUtil} from "../components/utils.js"
import List from "../components/list.js"
import Inventory, {InventorySkeleton, initInventory} from "../components/inventory.js"
import App from "../components/app.js"

if (authenticate()) {
  let app = new App({}, (app) => {
    initInventory(app);
    let list = new List(app.elements.contentWrapper, {skeletonFunction: InventorySkeleton.drawList, loadOnConstruct: false}, handleListChange);
    app.addUserControl(list);
    Util.RequestAsync("GET", app.adapiUrl("/api/inventory/stats"), (data) => {
      list.entitiesCount = data.count;
      app.loadControlValues("list");
      list.load();
    });
    if (app.user["access-level"] >= 4) {
      let container = Util.createElement("div", "ad-inv-new-container");
      app.elements.contentWrapper.createChild("button", "btn btn-block btn-outline-secondary", "+").addEvent("click", () => {
        container.removeChildren();
        let inv = container.createChild("form", "ad-inv-new");
        inv.customValidate(((e) => {
          console.log("process");
          e.preventDefault();
          let d = inv.process();
          Util.RequestAsync("POST", app.adapiUrl("/api/inventory/create"), (ret) => {
            console.log(ret)
          }, { data: [{
            name : d["name"],
            type: d["type"],
            state: d["state"],
            description: d["description"],
            extra: {},
            owner: d["owner"],
            location: d["location"],
            labels: []
          }]});
        }));
        let newInv = new Inventory({});
        inv.createChildWithAttributes("input", "form-control", null, {name: "name", placeholder: "name", required: "", type: "text"});
        inv.createChildWithAttributes("input", "form-control", null, {name: "type", placeholder: "type", required: "", type: "text"});
        inv.createChildWithAttributes("input", "form-control", null, {name: "state", placeholder: "state" , type: "text"});
        inv.createChildWithAttributes("input", "form-control", null, {name: "description", placeholder: "description" , type: "text"});
        inv.createChildWithAttributes("input", "form-control", null, {name: "owner", placeholder: "owner", type: "text"});
        inv.createChildWithAttributes("input", "form-control", null, {name: "location", placeholder: "location" , type: "text"});
        inv.createChildWithAttributes("input", "btn btn-success btn-block", null, {name: "submit", value: "add item", type: "submit"});
      });
      app.elements.contentWrapper.appendChild(container);
    }
  });

  function handleListChange(vals) {
    let dat = {from: vals.listPageNumber * vals.listPageEntitiesCount, count: vals.listPageEntitiesCount}
    Util.RequestAsync("POST", app.adapiUrl("/api/inventory"), (data) => {
      if (data != null) {
        if (data.Errors) {
            data.Errors.forEach(error => {
            app.notification.createNewError(error);
            });
        } else {
            vals.list.removeEntities();
            data.forEach(item => {
            app.Entities[item._adid] =  new Inventory(item);
            vals.list.addEntity(app.Entities[item._adid].drawList());
            });
        }
      } else {
        vals.list.removeEntities();
      }
    }, dat);
  }
}