import authenticate from "../components/auth.js"
import Util, {Link} from "../components/utils.js"
import List from "../components/list.js"
import Issue, {IssueSkeleton, initIssue} from "../components/issue.js"
import Ticket, {TicketSkeleton, initTicket} from "../components/ticket.js"
import App from "../components/app.js"
import DateTime from "../components/datetime.js";

if (authenticate()) {
  let app = new App();
  initIssue(app);
  initTicket(app);

  console.log(app.filter)
  console.log(app.config.app)
  console.log(app.page)

  if (app.page == "edit") {
    if (app.filter.ticket) {
      editTicket();
    } else if (app.filter.issue) {
      editIssue();
    } else {
      app.notification.createNew("error", "error", "ops. something went wrong. We dont know what you want us to do. Could you please start from the begining?")
    }
  } else {
    if (app.filter.ticket) {
      convertTicket();
    } else {
      newIssue();
    }
  }




  function editTicket() {
    let skel = TicketSkeleton.drawWindow();
    app.elements.contentWrapper.appendChild(skel);
    let ticket = app.Util.loadResource(app.filter["ticket"]["id"], Ticket, skel, "drawWindowEdit")
    console.log("editTicket", ticket)
  }

  function editIssue() {
    let skel = IssueSkeleton.drawWindow();
    app.elements.contentWrapper.appendChild(skel);
    let issue = app.Util.loadResource(app.filter["issue"]["id"], Issue, skel, "drawWindowEdit")
    console.log("editIssue")
  }

  function convertTicket() {
    let skel = IssueSkeleton.drawWindow();
    app.elements.contentWrapper.appendChild(skel);
    app.Util.RequestAsync("GET", app.adapiUrl("/api/id/" + app.filter["ticket"]["id"]), (data) => {
        let issueData = {
          title: data[0].ticket.title,
          description: `${data[0].ticket.description}
room: ${data[0].ticket.room}
involved items: ${data[0].ticket.involved_items}
`,
          weight: data[0].ticket.severity,
          time: {
            spent: [],
            due: -1,
            opened: Math.round((new Date()).getTime() / 1000),
            closed: -1,
            assigned: -1,
            working: -1,
            reviewed: -1
          }
        }
        console.log("convertTicket", issueData)
        let issue = new Issue(issueData);
        skel.parentNode.replaceChild(issue.drawNew(), skel);
      }
    );
  }

  function newIssue() {
    let skel = IssueSkeleton.drawWindow();
    app.elements.contentWrapper.appendChild(skel);
    let issueData = {
      title: "",
      description: "",
      weight: 0,
      time: {
        spent: [],
        due: -1,
        opened: Math.round((new Date()).getTime() / 1000),
        closed: -1,
        assigned: -1,
        working: -1,
        reviewed: -1
      }
    }
    let issue = new Issue(issueData);
    skel.parentNode.replaceChild(issue.drawNew(), skel);
  }
}