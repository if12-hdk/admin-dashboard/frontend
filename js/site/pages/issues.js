import authenticate from "../components/auth.js"
import Util, {Link} from "../components/utils.js"
import List from "../components/list.js"
import Issue, {IssueSkeleton, initIssue} from "../components/issue.js"
import App from "../components/app.js"

if (authenticate()) {
  let app = new App({}, (app) => {
    initIssue(app);

    let list = new List(app.elements.contentWrapper, {skeletonFunction: IssueSkeleton.drawList, loadOnConstruct: false, inlineControls:[{type:"checkbox", name: "issue-closed", label: "show closed issues", onchange: true}]}, handleListChange);
    app.addUserControl(list);
    Util.RequestAsync("GET", app.adapiUrl("/api/issues/stats"), (data) => {
      list.entitiesCount = data.count;
      app.loadControlValues("list");
      list.load();
    });
  });

  function handleListChange(vals) {
    let find = {};
    if (!vals.form["issue-closed"]) {
      find["open"] = true;
    }
    let search = vals.form["search"];
    if (search != "") {
      find["$or"] = [{title:{$regex:`.*${search}.*`, $options: "i"}}, {description:{$regex:`.*${search}.*`, $options: "i"}}]
    }
    let dat = {from: vals.listPageNumber * vals.listPageEntitiesCount, count: vals.listPageEntitiesCount, find: find}
    Util.RequestAsync("POST", app.adapiUrl("/api/issues"), (data) => {
      if (data != null) {
        if (data.Errors) {
          data.Errors.forEach(error => {
            app.notification.createNewError(error);
          });
        } else {
          vals.list.removeEntities();
          data.forEach(issue => {
            app.Entities[issue._adid] =  new Issue(issue);
            vals.list.addEntity(app.Entities[issue._adid].drawList());
          });
        }
      } else {
        vals.list.removeEntities();
      }
    }, dat);
  }
}